# Repo for ansible roles

There are a handful of roles and playbooks in this repo.
These serve to partially automate the setup of my personal Systems. Additionally, they help with keeoing configuration drift in check.
These roles are tested to work with Debian 12.

## Inventory
[`example-inv.yml`](./example-inv.yml) is an example inventory, that illustrates how to configure hosts.

## Roles

### [Basics](./roles/basics/tasks/main.yml)

Takes care of locales and basic packages.
And other stuff:
    - adding users and ssh keys
    - turning off password login
    - set up some very basic monitoring
    - set up msmtp

Variables that should be populated:  
`users` : list of password hashes and user names.  
`ssh_keys` : list of public keys and user names. Keys are added to authorized_keys file for specified user.  
`mail_server` : FQDN of mailserver for outgoing mail.  
`smptp_port` : Smtp port for outgoing mail.  
`host_mail_addr` : "From" address outgoing mail.  
`host_mail_user` : Username for login on mail server.  
`mail_passwd` : Password for login on mailserver. This string will be written to conf file. # TODO: use keyring instead.  
`disable_ssh_pw` : boolean  


### [Docker](./roles/docker/tasks/main.yml)

- Adds official docker repo + gpg key
- Sets a cleanup cron job for unused images
- sets storage driver (overlay2 is default)
- use `docker_storage_driver` variable to set the storage driver
- install docker (with compose plugin)

### [Iptables](./roles/iptables/tasks/main.yml)

Sets up some firewall rules.
Only supports configuring the INPUT and OUTPUT chains.
Other chains are not touched, in order to not interfere with docker and wireguard.

Port 22 is open by default, so I don't lock myself out of SSH.

Additonal rules may be specified as follows:
```
application_rules_V4:
        - "#Haproxy"
        - "-A INPUT -p tcp -m state --state NEW --dport 80 -j ACCEPT"
        - "-A INPUT -p tcp -m state --state NEW --dport 443 -j ACCEPT"
        - "#Mail"
        - "-A INPUT -m state --state NEW -m tcp -p tcp --dport 587 -j ACCEPT"
        - "-A INPUT -m state --state NEW -m tcp -p tcp --dport 465 -j ACCEPT"
application_rules_V6:
        - "#Haproxy"
        - "-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT"
        - "-A INPUT -m state --state NEW -m tcp -p tcp --dport 443 -j ACCEPT"
        - "#Mail"
        - "-A INPUT -m state --state NEW -m tcp -p tcp --dport 587 -j ACCEPT"
        - "-A INPUT -m state --state NEW -m tcp -p tcp --dport 465 -j ACCEPT"
```

### [Netdata](./roles/netdata/tasks/main.yml)

Installs Netdata. Sets bind adress to 127.0.0.1.  
Use Haproxy to make it availabe. Don't forget to add basic auth.  
Email alerts are sent to `monitoring_recipient`.  
Msmtp needs to be setup (by the basics role) for this to work.

### [wg-quick](./roles/wg-quick/tasks/main.yml)

Sets up wireguard.
Peers are specified as follows:
```
wg_peers:
        - {Name: 'Alice.example.com', PublicKey: 'asdf', AllowedIPs: '10.42.69.70/32, fdc9:beef:69:9ee9::70/128' , PersistentKeepalive: '25'}
        - {Name: 'Bob.example.com', PublicKey: 'fdsa', AllowedIPs: '10.42.69.68/32, fdc9:beef:69:9ee9::68/128' , PersistentKeepalive: '25'}
```

### [zed](./roles/zed/tasks/main.yml)

Sets up the ZFS event daemon. Setup of ZFS itself is not handled in this repo.  
Sends emails upon completed scrubs and other events.

### [Haproxy](./roles/haproxy/tasks/main.yml)

Installs Haproxy and lego. Config files are not templated.  
Ensures existence of dhparms.

Location of conf file must be provided as: `haproxy_conf_file: "file/path"`.
